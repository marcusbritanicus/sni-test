/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * This file was originally taken from LXQt project
 * <https://github.com/lxqt/lxqt-panel>
 * Several modifications have been made to suit the needs of DFL.
 * Any and all bug reports are to be filed with DFL and not LXQt.
 *
 * This library implements the StatusNotifierItem specifications.
 * https://www.freedesktop.org/wiki/Specifications/StatusNotifierItem/
 * DFL::StatusNotifierWatcher class provides the code for the watcher.
 **/

#include <QtCore>
#include <QDebug>
#include <QDBusConnectionInterface>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cstdio>

#include "StatusNotifierWatcher.hpp"
#include "StatusNotifierWatcherKDE.hpp"
#include "StatusNotifierWatcherFDO.hpp"

DFL::StatusNotifierWatcher::StatusNotifierWatcher() : QObject() {
    kde = new StatusNotifierWatcherKDE();
    fdo = new StatusNotifierWatcherFDO();

    if ( not kde->isServiceRunning() ) {
        qDebug() << "Unable to register Service:" << "org.kde.StatusNotifierWatcher";
    }

    if ( not kde->isObjectRegistered() ) {
        qDebug() << "Unable to register Object:" << "/StatusNotifierWatcher" << "on" << "org.kde.StatusNotifierWatcher";
    }

    if ( not fdo->isServiceRunning() ) {
        qDebug() << "Unable to register Service:" << "org.freedesktop.StatusNotifierWatcher";
    }

    if ( not fdo->isObjectRegistered() ) {
        qDebug() << "Unable to register Object:" << "/StatusNotifierWatcher" << "org.freedesktop.StatusNotifierWatcher";
    }
}


DFL::StatusNotifierWatcher::~StatusNotifierWatcher() {
    delete kde;
    delete fdo;
}
