/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * This file was originally taken from LXQt project
 * <https://github.com/lxqt/lxqt-panel>
 * Several modifications have been made to suit the needs of DFL.
 * Any and all bug reports are to be filed with DFL and not LXQt.
 *
 * This library implements the StatusNotifierItem specifications.
 * https://www.freedesktop.org/wiki/Specifications/StatusNotifierItem/
 * DFL::StatusNotifierWatcher class provides the code for the watcher.
 **/

#pragma once

#include <QDBusConnection>
#include <QDBusContext>
#include <QDBusMessage>
#include <QDBusMetaType>
#include <QDBusServiceWatcher>

namespace DFL {
    class StatusNotifierWatcher;
}

class StatusNotifierWatcherKDE;
class StatusNotifierWatcherFDO;

class DFL::StatusNotifierWatcher : public QObject {
    Q_OBJECT

    public:
        StatusNotifierWatcher();

        ~StatusNotifierWatcher();

    private:
        StatusNotifierWatcherKDE *kde;
        StatusNotifierWatcherFDO *fdo;
};
