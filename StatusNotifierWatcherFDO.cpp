/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * This file was originally taken from LXQt project
 * <https://github.com/lxqt/lxqt-panel>
 * Several modifications have been made to suit the needs of DFL.
 * Any and all bug reports are to be filed with DFL and not LXQt.
 *
 * This library implements the StatusNotifierItem specifications.
 * https://www.freedesktop.org/wiki/Specifications/StatusNotifierItem/
 * StatusNotifierWatcherFDO class provides the code for the watcher.
 **/

#include <QtCore>
#include <QDebug>
#include <QDBusConnectionInterface>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cstdio>

#include "StatusNotifierWatcherFDO.hpp"

StatusNotifierWatcherFDO::StatusNotifierWatcherFDO() : QObject() {
    QDBusConnection dbus = QDBusConnection::sessionBus();

    serviceRunning   = dbus.registerService( "org.freedesktop.StatusNotifierWatcher" );
    objectRegistered = dbus.registerObject( "/StatusNotifierWatcher", this, QDBusConnection::ExportScriptableContents );

    mWatcher = new QDBusServiceWatcher( this );
    mWatcher->setConnection( dbus );
    mWatcher->setWatchMode( QDBusServiceWatcher::WatchForUnregistration );

    connect( mWatcher, &QDBusServiceWatcher::serviceUnregistered, this, &StatusNotifierWatcherFDO::serviceUnregistered );
}


StatusNotifierWatcherFDO::~StatusNotifierWatcherFDO() {
    QDBusConnection::sessionBus().unregisterService( "org.freedesktop.StatusNotifierWatcher" );
}


bool StatusNotifierWatcherFDO::isServiceRunning() {
    return serviceRunning;
}


bool StatusNotifierWatcherFDO::isObjectRegistered() {
    return objectRegistered;
}


void StatusNotifierWatcherFDO::RegisterStatusNotifierItem( const QString& serviceOrPath ) {
    QString service = serviceOrPath;
    QString path    = "/StatusNotifierItem";

    // workaround for sni-qt
    if ( service.startsWith( '/' ) ) {
        path    = service;
        service = message().service();
    }

    QString notifierItemId = service + path;

    if ( QDBusConnection::sessionBus().interface()->isServiceRegistered( service ).value() && !mServices.contains( notifierItemId ) ) {
        mServices << notifierItemId;
        mWatcher->addWatchedService( service );
        emit StatusNotifierItemRegistered( notifierItemId );
    }
}


void StatusNotifierWatcherFDO::RegisterStatusNotifierHost( const QString& service ) {
    if ( !mHosts.contains( service ) ) {
        mHosts.append( service );
        mWatcher->addWatchedService( service );
    }
}


void StatusNotifierWatcherFDO::serviceUnregistered( const QString& service ) {
    mWatcher->removeWatchedService( service );

    if ( mHosts.contains( service ) ) {
        mHosts.removeAll( service );
        return;
    }

    QString               match = service + '/';
    QStringList::Iterator it    = mServices.begin();

    while ( it != mServices.end() ) {
        if ( it->startsWith( match ) ) {
            QString name = *it;
            it = mServices.erase( it );
            emit StatusNotifierItemUnregistered( name );
        }

        else {
            ++it;
        }
    }
}
