/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *
 * This example demonstrates how to write a simple status notifier
 * watcher daemon. Be sure to have DFL::SNI library installed
 **/

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <QtCore>
#include <QDebug>
#include <QDBusConnectionInterface>

#include "StatusNotifierWatcher.hpp"
#include "StatusNotifierWatcherKDE.hpp"
#include "StatusNotifierWatcherFDO.hpp"

int main( int argc, char *argv[] ) {
    QCoreApplication app( argc, argv );

    app.setOrganizationName( "DFL" );
    app.setApplicationName( "StatusNotifier Watcher" );
    app.setApplicationVersion( PROJECT_VERSION );

    DFL::StatusNotifierWatcher *sniw = new DFL::StatusNotifierWatcher();

    qDebug() << "Services org.kde.StatusNotifierWatcher and org.freedesktop.StatusNotifierWatcher running...";
    qDebug() << "Object /StatusNotifierWatcher registered...";
    qDebug() << "Ready for incoming connections...";

    return app.exec();
}
